package com.epam.gomel.tat.lesson_12.reporting;

import com.epam.gomel.tat.lesson_12.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

public class TestNgListener implements IResultListener2 {

    @Override
    public void onStart(ITestContext context) {
        Logger.info("START : " + context.getCurrentXmlTest().getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        Logger.info("FINISH : " + context.getName());
        Browser.kill();
    }

    @Override
    public void onTestStart(ITestResult result) {
        Logger.info("TEST START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Logger.info("TEST METHOD SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Logger.info("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.error("TEST METHOD FAILED :", result.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        Logger.info("TEST METHOD SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.error("TEST METHOD SKIPPED : ", result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        Logger.info("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.error("TEST METHOD FAILED : ", result.getThrowable());
    }

    @Override
    public void beforeConfiguration(ITestResult result) {
        Logger.info("CONFIG START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onConfigurationSuccess(ITestResult result) {
        Logger.info("CONFIG SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onConfigurationFailure(ITestResult result) {
        Logger.info("CONFIG FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.error("CONFIG FAILED", result.getThrowable());
    }

    @Override
    public void onConfigurationSkip(ITestResult result) {
        Logger.info("CONFIG SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }
}
