package com.epam.gomel.tat.lesson_12.bo.common;

public class AccountBuilder {

/*    private static final String DEFAULT_LOGIN = "igorepamtest";
    private static final String DEFAULT_PASSWORD = "qwertyqwerty";
    private static final String INCORRECT_PASSWORD = "beleberda223634735";*/
    private static final String EMAIL = "igorepamtest@yandex.ru";

    public static Account createUser() {
        return new Account();
    }

    public static Builder build(Account user) {
        return new Builder(user);
    }

    public static class Builder {
        Account accountToBuild;

        public Builder(Account user) {
            accountToBuild = user;
        }

        public Builder withCorrectLogin(String login) {
            accountToBuild.setLogin(login);
            return this;
        }

        public Builder withCorrectPassword(String password) {
            accountToBuild.setPassword(password);
            return this;
        }

        public Builder withIncorrectPassword(String password) {
            accountToBuild.setPassword(password);
            return this;
        }

        public Builder withEmail() {
            accountToBuild.setEmail(EMAIL);
            return this;
        }
    }
}
