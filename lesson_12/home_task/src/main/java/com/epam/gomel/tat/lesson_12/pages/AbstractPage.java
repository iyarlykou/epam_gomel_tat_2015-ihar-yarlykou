package com.epam.gomel.tat.lesson_12.pages;

import com.epam.gomel.tat.lesson_12.ui.Browser;

public abstract class AbstractPage {

    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.get();
    }

}
