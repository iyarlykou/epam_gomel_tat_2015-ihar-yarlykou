package com.epam.gomel.tat.lesson_12.pages;

import com.epam.gomel.tat.lesson_12.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_12.cli.GlobalOptions;
import com.epam.gomel.tat.lesson_12.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_12.pages.AbstractPage;
import com.epam.gomel.tat.lesson_12.reporting.Logger;
import com.epam.gomel.tat.lesson_12.utils.FileUtils;
import org.openqa.selenium.By;

import java.io.File;

public class LetterContentPage extends AbstractPage {

    public static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']");
    public static final By MAIL_CONTENT_LOCATOR = By.xpath("//div[@class='b-message__i1']//div[@class='b-message-body__content']/p");
    public static final By SENDER_EMAIL_LOCATOR = By.xpath("//span[@class='b-message-head__name']//span[@class='b-message-head__email']");
    public static final By MAIL_SUBJECT_LOCATOR = By.xpath("//span[@class='js-message-subject js-invalid-drag-target']");

    public void compareFiles(MailLetter letter) {
        browser.click(DOWNLOAD_BUTTON_LOCATOR);
        File attachFile = new File(letter.getAttach());
        File downloadedFile = new File(GlobalOptions.DOWNLOAD_DIR + attachFile.getName());
        FileUtils.waitForFile(downloadedFile);
        FileUtils.compareFileContent(attachFile, downloadedFile);
    }

    public void compareMailContent(MailLetter letter) {
        String mailToFormat = " <" + letter.getMailTo() + ">";
        String subject = browser.getText(MAIL_SUBJECT_LOCATOR);
        String content = browser.getText(MAIL_CONTENT_LOCATOR);
        String senderEmail = browser.getText(SENDER_EMAIL_LOCATOR);

        StringBuilder builder = new StringBuilder();
        if (!letter.getSubject().equals(subject)) {
            builder.append("Mail subjects is not equals  " + "Expected subject: " + letter.getSubject() + "  Actual subject: " + subject);
        }
        if (!letter.getMailContent().equals(content)) {
            builder.append("Mail contents is not equals  " + "Expected content: " + letter.getMailContent() + "  Actual content: " + content);
        }
        if (!mailToFormat.equals(senderEmail)) {
            builder.append("Emails is not equals  " + "Expected email: " + mailToFormat + "  Actual email: " + senderEmail);
        }
        if (!builder.toString().equals("")) {
            Logger.error(builder.toString());
            throw new TestCommonRuntimeException(builder.toString());
        }
    }
}
