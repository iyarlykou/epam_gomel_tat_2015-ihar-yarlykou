package com.epam.gomel.tat.lesson_12.steps;

import com.epam.gomel.tat.lesson_12.bo.common.Account;
import com.epam.gomel.tat.lesson_12.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_12.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_12.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.lesson_12.service.MailGuiService;
import com.epam.gomel.tat.lesson_12.service.MailLoginService;
import com.epam.gomel.tat.lesson_12.ui.Browser;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

public class SendMailSteps extends Steps {

    private Account user;
    private MailLoginService loginService = new MailLoginService();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = MailLetterBuilder.getDefaultMailLetterWithoutAttach();

    @Given("Actor is Mailbox User")
    public void actorIsMailBoxOwner() {
        user = AccountBuilder.createUser();
    }

    @Given("Actor has correct login $login and correct password $password")
    public void actorHasCorrectLoginAndPassword(String login, String password) {
        AccountBuilder.build(user).withCorrectLogin(login).withCorrectPassword(password).withEmail();
    }

    @Given("Actor login to mailbox")
    public void actorLoginToMailbox() {
        loginService.doLogin(user);
    }

    @When("Actor send mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Then("Actor see mail in sending list")
    public void checkMailInSendList() {
        mailGuiService.checkMailInSentList(letter);
    }

    @AfterStory
    public void afterStory() {
        Browser.kill();
    }
}
