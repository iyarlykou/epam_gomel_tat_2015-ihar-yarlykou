package com.epam.gomel.tat.lesson_12.utils;

import com.epam.gomel.tat.lesson_12.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_12.reporting.Logger;
import com.google.common.base.Function;
//import com.sun.istack.internal.Nullable;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class FileUtils {
    public static int WAIT_FILE_DOWNLOAD_SECONDS = 30;
    public static final int FILE_NAME_LENGTH = 10;
    public static final int FILE_CONTENT_LENGTH = 60;

    public static void waitForFile(File filename) {
        FluentWait wait = new FluentWait<File>(filename).withTimeout(WAIT_FILE_DOWNLOAD_SECONDS, TimeUnit.SECONDS);
        wait.until(new Function<File, Boolean>() {
            // @Nullable
            @Override
            public Boolean apply(File file) {
                return file.exists();
            }
        });
    }

    public static String getRandomFileContent() {
        return RandomStringUtils.randomAlphanumeric(FILE_CONTENT_LENGTH);
    }

    public static String getRandomFileName() {
        return RandomStringUtils.randomAlphanumeric(FILE_NAME_LENGTH);
    }

    public static File createRandomFile() {

        String fileName = getRandomFileName() + ".txt";
        String fileContent = getRandomFileContent();
        File file = new File(fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
                org.apache.commons.io.FileUtils.writeStringToFile(file, fileContent);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    public static void compareFileContent(File file1, File file2) {
        try {
            org.apache.commons.io.FileUtils.contentEquals(file1, file2);
        } catch (IOException e) {
            Logger.error("Files content is not equals");
            throw new TestCommonRuntimeException("Files content is not equals");
        }
    }

    public static String getResource(String filePath) {
        return FileUtils.class.getClassLoader().getResource(filePath).getPath();
    }

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = FileUtils.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
        }
        return null;
    }
}
