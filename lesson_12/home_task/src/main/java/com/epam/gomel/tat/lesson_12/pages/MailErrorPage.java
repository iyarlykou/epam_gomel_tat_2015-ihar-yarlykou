package com.epam.gomel.tat.lesson_12.pages;

import com.epam.gomel.tat.lesson_12.exception.TestCommonRuntimeException;
import org.openqa.selenium.By;

public class MailErrorPage extends AbstractPage {

    static final String ERROR_TEXT = "Неправильная пара логин-пароль! Авторизоваться не удалось.";
    private static final By ERROR_TEXT_LOCATOR = By.cssSelector("div.error-msg");

    public void checkErrorMessagePresent() {
        browser.waitForVisible(ERROR_TEXT_LOCATOR);
        String errorMessage = browser.getText(ERROR_TEXT_LOCATOR);
        if (!errorMessage.equals(ERROR_TEXT)) {
            throw new TestCommonRuntimeException("Error message is not found. Current message: " + errorMessage);
        }
    }
}
