package com.epam.gomel.tat.lesson_12.service;

import com.epam.gomel.tat.lesson_12.bo.common.Account;
import com.epam.gomel.tat.lesson_12.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_12.pages.MailLoginPage;
import com.epam.gomel.tat.lesson_12.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_12.reporting.Logger;

public class MailLoginService {

    public void doLogin(Account user) {
        Logger.info("Login as : " + user);
        MailboxBasePage mailbox = new MailLoginPage().open().login(user.getLogin(), user.getPassword());
    }

    public void checkMailListAccessible(Account user) {
        Logger.info("Mail list is available to user " + user);
        MailboxBasePage mailbox = new MailboxBasePage();
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null || !userEmail.equals(user.getEmail())) {
            Logger.error("Login failed. User Mail : '" + userEmail + "'");
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }
}
