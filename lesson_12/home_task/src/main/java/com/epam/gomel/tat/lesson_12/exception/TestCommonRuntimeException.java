package com.epam.gomel.tat.lesson_12.exception;

public class TestCommonRuntimeException extends RuntimeException {

    public TestCommonRuntimeException(String s) {
        super(s);
    }

    public TestCommonRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
