package com.epam.gomel.tat.lesson_12.pages;

import org.openqa.selenium.By;

public class MailSentListPage extends AbstractPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and (@style='' or not(@style))]//a[.//span[@title='%s']]";

    public void openMailPage(String subject) {
        browser.click(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
    }
}
