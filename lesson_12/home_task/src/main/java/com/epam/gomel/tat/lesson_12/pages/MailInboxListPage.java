package com.epam.gomel.tat.lesson_12.pages;

import org.openqa.selenium.By;

public class MailInboxListPage extends AbstractPage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }
}
