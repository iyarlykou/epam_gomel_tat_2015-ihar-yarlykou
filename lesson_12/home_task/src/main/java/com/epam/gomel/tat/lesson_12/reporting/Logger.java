package com.epam.gomel.tat.lesson_12.reporting;

public class Logger {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);

    public synchronized static void error(Object message) {
        log.error(message);
    }

    public synchronized static void error(Object message, Throwable t) {
        log.error(message, t);
    }

    public synchronized static void info(Object message) {
        log.info(message);
    }

    public synchronized static void info(Object message, Throwable t) {
        log.info(message, t);
    }

    public synchronized static void trace(Object message) {
        log.trace(message);
    }

    public synchronized static void trace(Object message, Throwable t) {
        log.trace(message, t);
    }

    public synchronized static void debug(Object message) {
        log.debug(message);
    }

    public synchronized static void debug(Object message, Throwable t) {
        log.debug(message, t);
    }

    public synchronized static void fatal(Object message) {
        log.fatal(message);
    }

    public synchronized static void fatal(Object message, Throwable t) {
        log.fatal(message, t);
    }

    public synchronized static void warn(Object message) {
        log.warn(message);
    }

    public synchronized static void warn(Object message, Throwable t) {
        log.warn(message, t);
    }
}
