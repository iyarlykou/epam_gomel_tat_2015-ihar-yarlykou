package com.epam.gomel.tat.lesson_12.pages;

import org.openqa.selenium.By;

public class MailLoginPage extends AbstractPage {

    public static final String BASE_URL = "http://mail.yandex.ru";
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//span[contains(@class, 'auth-form')]/button");

    public MailLoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.click(LOGIN_BUTTON_LOCATOR);
        return new MailboxBasePage();
    }
}
