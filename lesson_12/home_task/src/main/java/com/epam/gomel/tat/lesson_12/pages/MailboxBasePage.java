package com.epam.gomel.tat.lesson_12.pages;

import org.openqa.selenium.By;

public class MailboxBasePage extends AbstractPage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final By USER_MAIL_LOCATOR = By.className("header-user-name");
    private static final By ERROR_TEXT_LOCATOR = By.cssSelector("div.error-msg");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(SENT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSentListPage();
    }

    public MailboxBasePage open() {
        browser.open(MailLoginPage.BASE_URL);
        return new MailboxBasePage();
    }

    public String getUserEmail() {
        return browser.getText(USER_MAIL_LOCATOR);
    }

    public String getMessage() {
        return browser.getText(ERROR_TEXT_LOCATOR);
    }
}
