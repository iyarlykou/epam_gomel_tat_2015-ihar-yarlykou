package com.epam.gomel.tat.lesson_12.steps;

import com.epam.gomel.tat.lesson_12.bo.common.Account;
import com.epam.gomel.tat.lesson_12.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_12.pages.MailErrorPage;
import com.epam.gomel.tat.lesson_12.service.MailLoginService;
import com.epam.gomel.tat.lesson_12.ui.Browser;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

public class UnSuccessMailLoginSteps extends Steps {

    private MailLoginService loginService = new MailLoginService();
    private Account user;

    @Given("Actor is User")
    public void actorIsMailBoxOwner() {
        user = AccountBuilder.createUser();
    }

    @Given("Actor has correct login $login and incorrect password $password")
    public void actorHasCorrectLoginAndPassword(String login, String password) {
        AccountBuilder.build(user).withCorrectLogin(login).withIncorrectPassword(password).withEmail();
    }

    @When("Actor login to mailbox with incorrect data")
    public void actorLoginToMailbox() {
        loginService.doLogin(user);
    }

    @Then("Actor haven't access to mailbox and see error message")
    public void checkErrorMessage() {
        new MailErrorPage().checkErrorMessagePresent();
    }

    @AfterStory
    public void afterStory() {
        Browser.kill();
    }
}
