package com.epam.gomel.tat.lesson_12.steps;

import com.epam.gomel.tat.lesson_12.bo.common.Account;
import com.epam.gomel.tat.lesson_12.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_12.service.MailLoginService;
import com.epam.gomel.tat.lesson_12.ui.Browser;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

public class SuccessMailLoginSteps extends Steps {

    private MailLoginService loginService = new MailLoginService();
    private Account user;

    @Given("User")
    public void actorIsUser() {
        user = AccountBuilder.createUser();
    }

    @Given("User has correct login $login and correct password $password")
    public void userHasCorrectLoginAndPassword(String login, String password) {
        AccountBuilder.build(user).withCorrectLogin(login).withCorrectPassword(password).withEmail();
    }

    @When("User login to mailbox")
    public void LoginToMailbox() {
        loginService.doLogin(user);
    }

    @Then("User has access to mail list")
    public void checkAccessToMailList() {
        loginService.checkMailListAccessible(user);
    }

    @AfterStory
    public void afterStory() {
        Browser.kill();
    }
}
