package com.epam.gomel.tat.lesson_12.utils;

public enum ParallelMode {
    FALSE("false"),
    TESTS("tests"),
    CLASSES("classes"),
    METHODS("methods"),
    SUITES("suites");

    private String alias;

    private ParallelMode(String alias) {
        this.alias = alias;
    }

    public static ParallelMode getTypeByAlias(String alias) {
        for (ParallelMode type : ParallelMode.values()) {
            if (type.getAlias().equals(alias.toLowerCase())) {
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getAlias() {
        return alias;
    }
}
