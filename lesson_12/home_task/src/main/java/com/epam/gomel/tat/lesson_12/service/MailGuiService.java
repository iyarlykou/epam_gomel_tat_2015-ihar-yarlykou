package com.epam.gomel.tat.lesson_12.service;

import com.epam.gomel.tat.lesson_12.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_12.pages.LetterContentPage;
import com.epam.gomel.tat.lesson_12.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_12.reporting.Logger;

public class MailGuiService {

    public void sendMail(MailLetter letter) {
        Logger.info("Sending mail to" + letter.getMailTo());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getMailTo(), letter.getSubject(), letter.getMailContent(), letter.getAttach());
    }

    public void checkMailInSentList(MailLetter letter) {
        Logger.info("Check mail in sent list");
        new MailboxBasePage().openSentPage().openMailPage(letter.getSubject());
        new LetterContentPage().compareMailContent(letter);
    }
}
