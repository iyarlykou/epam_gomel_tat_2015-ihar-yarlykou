Feature : Success send mail

Meta:
@success_send_mail

Narrative:
As a Mailbox User
I want to login to my mailbox and send mail
So that User have access to mailbox list, sending mail and see that in sending list

Scenario: Success send mail
Given Actor is Mailbox User
And Actor has correct login igorepamtest and correct password qwertyqwerty
And Actor login to mailbox
When Actor send mail
Then Actor see mail in sending list