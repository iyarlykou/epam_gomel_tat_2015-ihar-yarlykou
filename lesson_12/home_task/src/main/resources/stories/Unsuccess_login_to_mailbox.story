Feature : Unsuccess login to mailbox

Meta:
@unsuccess_login

Narrative:
As a Mailbox User
I want to login to my mailbox with incorrect password
So that User have not access to my mail list and see error message

Scenario: Unsuccess login to mailbox
Given Actor is User
And Actor has correct login igorepamtest and incorrect password beleberda223634735
When Actor login to mailbox with incorrect data
Then Actor haven't access to mailbox and see error message