Feature : Success login to mailbox

Meta:
@success_login

Narrative:
As a Mailbox User
I want to login to my mailbox
So that User have access to my mail list

Scenario: Succes login as mailbox owner
Given User
And User has correct login igorepamtest and correct password qwertyqwerty
When User login to mailbox
Then User has access to mail list