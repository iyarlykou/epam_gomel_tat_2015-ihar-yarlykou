package com.epam.tat.lesson9.utils;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class Logger {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("console");

    public synchronized static void info(String message) {
        logger.info(message);
    }

    public synchronized static void debug(String message) {
        logger.debug(message);
    }

    public synchronized static void error(String message, Exception e) {
        logger.error(message, e);
    }
}
