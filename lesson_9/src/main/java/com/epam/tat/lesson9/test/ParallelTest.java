package com.epam.tat.lesson9.test;

import com.epam.tat.lesson9.GlobalConfig;
import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;

/**
 * Created by Aleh_Vasilyeu on 3/31/2015.
 */
public class ParallelTest extends BaseTest {

@BeforeSuite
 public void befSuite(ITestContext context) {
   context.getSuite().getXmlSuite().setParallel(GlobalConfig.instance().getParallelMode().getAlias());
   context.getSuite().getXmlSuite().setThreadCount(GlobalConfig.instance().getThreadCount());
  }
}
