package com.epam.gomel.tat.lesson_9.runner;

import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import com.epam.gomel.tat.lesson_9.cli.GlobalOptions;
import com.epam.gomel.tat.lesson_9.reporting.SuiteListener;
import com.epam.gomel.tat.lesson_9.reporting.TestNgListener;
import com.epam.gomel.tat.lesson_9.reporting.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import org.testng.xml.Parser;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    private static GlobalOptions globalOptions = null;

    public static void main(String[] args) {
        globalOptions = GlobalOptions.getInstance();
        Runner runner = new Runner();
        runner.parseCli(args);
        runner.runTests();
    }

    private void parseCli(String[] args) {

        CmdLineParser parser = new CmdLineParser(globalOptions);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            Logger.error("Can't parse arguments! " + e.getMessage());
            parser.printUsage(System.err);
            System.exit(1);
        }
    }

    private void runTests() {

        TestNG testNG = new TestNG();
        testNG.addListener(new TestNgListener());
        testNG.addListener(new SuiteListener());
        testNG.addListener(new ConfigurationListener());
        testNG.addListener(new MethodListener());
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        for (String suiteFile : GlobalOptions.getSuitesFiles()) {
            try {
                List<XmlSuite> suitesToConfigure = (List<XmlSuite>) (new Parser(suiteFile).parse());
                for (XmlSuite suiteToConfigure : suitesToConfigure) {
                    suites.add(suiteToConfigure);
                }
            } catch (Exception e) {
                Logger.error("Can't set options to suites files!");
                e.printStackTrace();
            }
        }
        testNG.setXmlSuites(suites);
        testNG.run();
    }
}
