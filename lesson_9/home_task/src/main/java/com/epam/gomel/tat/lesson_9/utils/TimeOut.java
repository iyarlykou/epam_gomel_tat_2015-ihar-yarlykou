package com.epam.gomel.tat.lesson_9.utils;

import com.epam.gomel.tat.lesson_9.reporting.Logger;

public class TimeOut {

    public static void sleep(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            Logger.info("sleep interrupted");
        }
    }
}
