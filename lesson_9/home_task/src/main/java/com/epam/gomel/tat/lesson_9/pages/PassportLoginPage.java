package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

public class PassportLoginPage extends AbstractPage {

    public static final String PASSPORT_URL = "https://passport.yandex.ru";
    private static final By PASSPORT_LOGIN_INPUT_LOCATOR = By.id("login");
    private static final By PASSPORT_PASSWORD_INPUT_LOCATOR = By.id("passwd");
    private static final By PASSPORT_LOGIN_BUTTON_LOCATOR = By.xpath("//button[@type='submit']");

    public PassportLoginPage open() {
        browser.open(PASSPORT_URL);
        return this;
    }

    public void login(String login, String password) {
        browser.waitForPresent(PASSPORT_LOGIN_INPUT_LOCATOR);
        browser.type(PASSPORT_LOGIN_INPUT_LOCATOR, login);
        browser.waitForPresent(PASSPORT_PASSWORD_INPUT_LOCATOR);
        browser.type(PASSPORT_PASSWORD_INPUT_LOCATOR, password);
        browser.waitForPresent(PASSPORT_LOGIN_BUTTON_LOCATOR);
        browser.click(PASSPORT_LOGIN_BUTTON_LOCATOR);
    }
}
