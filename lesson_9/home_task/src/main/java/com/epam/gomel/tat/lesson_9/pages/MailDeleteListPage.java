package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

public class MailDeleteListPage extends AbstractPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and (@style='' or not(@style))]//a[.//span[@title='%s']]";

    public boolean isMessagePresentInDeleteList(String subject) {
        try {
            browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
