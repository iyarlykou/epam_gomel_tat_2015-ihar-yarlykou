package com.epam.gomel.tat.lesson_9.reporting;

import com.epam.gomel.tat.lesson_9.cli.GlobalOptions;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalOptions.getInstance().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalOptions.getInstance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {
    }
}
