package com.epam.gomel.tat.lesson_9.test;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.lesson_9.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_9.service.LoginGuiService;
import com.epam.gomel.tat.lesson_9.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SendMailWithAttachTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getDefaultMailLetterWithAttach();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail in sent list", dependsOnMethods = "sendMail")
    public void checkMailInSentList() {
        mailGuiService.checkMailInSentList(letter);
    }

    @Test(description = "Compare files", dependsOnMethods = "checkMailInSentList")
    public void checkFiles() {
        mailGuiService.compareAttach(letter);
    }

}
