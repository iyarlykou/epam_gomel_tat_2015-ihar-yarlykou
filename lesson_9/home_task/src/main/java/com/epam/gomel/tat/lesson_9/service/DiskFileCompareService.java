package com.epam.gomel.tat.lesson_9.service;

import com.epam.gomel.tat.lesson_9.cli.GlobalOptions;
import com.epam.gomel.tat.lesson_9.utils.FileUtils;

import java.io.File;

public class DiskFileCompareService {

    public void compareFiles(File attachedFile) {

        File downloadedFile = new File(GlobalOptions.DOWNLOAD_DIR + attachedFile.getName());
        FileUtils.waitForFile(downloadedFile);
        FileUtils.compareFileContent(attachedFile, downloadedFile);
    }
}
