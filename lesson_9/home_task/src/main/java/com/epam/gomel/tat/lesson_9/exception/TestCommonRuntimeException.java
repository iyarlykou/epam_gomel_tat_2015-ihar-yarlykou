package com.epam.gomel.tat.lesson_9.exception;

public class TestCommonRuntimeException extends RuntimeException {

    public TestCommonRuntimeException(String s) {
        super(s);
    }

    public TestCommonRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
