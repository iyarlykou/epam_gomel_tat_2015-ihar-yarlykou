package com.epam.gomel.tat.lesson_9.pages;

import com.epam.gomel.tat.lesson_9.utils.TimeOut;
import org.openqa.selenium.By;

import java.io.File;

public class DiskTrashPage extends AbstractPage {

    private static final By TRASH_LOCATOR = By.xpath("//div[@data-id='/trash']");
    private static final String FILE_LINK_PATTERN = "//div[@class='ns-view-listing b-listing b-listing_view_icons b-listing_group-by_none ns-view-visible']//div[@title='%s']";
    private static final By RESTORE_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");

    public DiskTrashPage open() {
        browser.waitForVisible(TRASH_LOCATOR);
        browser.doubleClick(TRASH_LOCATOR);
        return new DiskTrashPage();
    }

    public void restoreFile(File attachedFile) {
        browser.waitForVisible(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())));
        browser.click(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())));
        TimeOut.sleep(2);
        browser.waitForVisible(RESTORE_BUTTON_LOCATOR);
        browser.click(RESTORE_BUTTON_LOCATOR);
    }
}
