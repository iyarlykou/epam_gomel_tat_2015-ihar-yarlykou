package com.epam.gomel.tat.lesson_9.test;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.service.DiskGuiService;
import com.epam.gomel.tat.lesson_9.service.DiskLoginGuiService;
import com.epam.gomel.tat.lesson_9.utils.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class DiskTest extends BaseTest {

    private DiskLoginGuiService diskLoginGuiService = new DiskLoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private DiskGuiService diskGuiService = new DiskGuiService();
    private static File uploadFile = FileUtils.createRandomFile();

    @Test(description = "Login to Yandex Disk")
    public void loginToAccount() {
        diskLoginGuiService.loginToDiskAccount(defaultAccount);
    }

    @Test(description = "Upload File", dependsOnMethods = "loginToAccount")
    public void uploadFile() {
        diskGuiService.uploadFile(uploadFile);
    }

    @Test(description = "Download and compare files", dependsOnMethods = "uploadFile")
    public void compareFiles() {
        diskGuiService.compareFiles(uploadFile);
    }

    @Test(description = "File To Trash", dependsOnMethods = "compareFiles")
    public void fileToTrash() {
        diskGuiService.moveFileToTrash(uploadFile);
    }

    @Test(description = "Remove File From Trash", dependsOnMethods = "fileToTrash")
    public void restoreFileFromTrash() {
        diskGuiService.restoreFileFromTrash(uploadFile);
        Assert.assertTrue(diskGuiService.isFileRestore(uploadFile));
    }
}
