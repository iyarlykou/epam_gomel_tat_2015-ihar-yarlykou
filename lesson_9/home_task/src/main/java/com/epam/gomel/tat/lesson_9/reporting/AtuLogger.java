package com.epam.gomel.tat.lesson_9.reporting;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;

public class AtuLogger {

    public static void info(String message) {
        info(message, false);
    }

    public static void info(String message, boolean needScreenshot) {
        Logger.info(message);
        ATUReports.add(message, LogAs.INFO, needScreenshot ? new CaptureScreen(
                CaptureScreen.ScreenshotOf.BROWSER_PAGE) : null);
    }
}
