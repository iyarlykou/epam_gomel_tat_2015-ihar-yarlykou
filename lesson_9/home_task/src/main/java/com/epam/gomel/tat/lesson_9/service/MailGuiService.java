package com.epam.gomel.tat.lesson_9.service;

import com.epam.gomel.tat.lesson_9.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_9.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_9.reporting.Logger;

public class MailGuiService {

    public void sendMail(MailLetter letter) {
        Logger.info("Sending mail to" + letter.getMailTo());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getMailTo(), letter.getSubject(), letter.getMailContent(), letter.getAttach());
    }

    public void checkMailInSentList(MailLetter letter) {
        Logger.info("Check mail in sent list");
        new MailboxBasePage().openSentPage().openMailPage(letter.getSubject());
        new CheckLetterContentService().compareMailContent(letter);
    }

    public boolean isMailInSpamList(MailLetter letter) {
        Logger.info("Check mail in spam list");
        MailboxBasePage mailbox = new MailboxBasePage();
        return mailbox.openSpamPage().isMessagePresentInSpamList(letter.getSubject());
    }

    public boolean isMailInDeleteList(MailLetter letter) {
        Logger.info("Check mail in delete list");
        MailboxBasePage mailbox = new MailboxBasePage();
        return mailbox.openDeletePage().isMessagePresentInDeleteList(letter.getSubject());
    }

    public boolean isMailInInboxList(MailLetter letter) {
        Logger.info("Check mail in inbox list");
        MailboxBasePage mailbox = new MailboxBasePage();
        return mailbox.openInboxPage().isMessagePresentInInboxList(letter.getSubject());
    }

    public void compareAttach(MailLetter letter) {
        Logger.info("Compare files");
        new MailboxBasePage().openSentPage().openMailPage(letter.getSubject());
        new CheckLetterContentService().compareFiles(letter);
    }
}
