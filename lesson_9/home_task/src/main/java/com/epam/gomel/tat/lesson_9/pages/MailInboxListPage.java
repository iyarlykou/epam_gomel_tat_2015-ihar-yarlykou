package com.epam.gomel.tat.lesson_9.pages;

import org.openqa.selenium.By;

public class MailInboxListPage extends AbstractPage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final By SPAM_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='tospam']");
    private static final String CHECKBOX_MAIL_LOCATOR_PATTERN = "//span[@title='%s']/ancestor::div[1]//input[@type='checkbox']";
    private static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='delete']");
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and (@style='' or not(@style))]//a[.//span[@title='%s']]";

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailboxBasePage messageToSpam(String subject) {
        browser.waitForVisible(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }

    public MailboxBasePage messageToDelete(String subject) {
        browser.waitForVisible(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(DELETE_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }

    public boolean isMessagePresentInInboxList(String subject) {
        try {
            browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
