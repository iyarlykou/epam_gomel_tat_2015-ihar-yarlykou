package com.epam.gomel.tat.lesson_9.pages;

import com.epam.gomel.tat.lesson_9.utils.TimeOut;
import org.openqa.selenium.By;

import java.io.File;

public class DiskBasePage extends AbstractPage {

    public static final String DISK_URL = "https://disk.yandex.ru/client/disk";
    private static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//input [@class='_nb-file-intruder-input']");
    private static final By CLOSE_BUTTON_LOCATOR = By.xpath("//div[@class='_nb-popup-i']/a");
    private static final String FILE_LINK_PATTERN = "//div[1][@title='%s']";
    private static final By TRASH_LOCATOR = By.xpath("//div[@data-id='/trash']");
    private static final String DOWNLOAD_BUTTON_PATTERN = "//div[@data-key='view=aside&idContext=/disk&id=/disk/%s']//button[@data-click-action='resource.download']";


    public DiskBasePage open() {
        browser.open(DISK_URL);
        return new DiskBasePage();
    }

    public void uploadFile(File attachedFile) {
        browser.waitForPresent(UPLOAD_BUTTON_LOCATOR);
        browser.attachFile(UPLOAD_BUTTON_LOCATOR, attachedFile.getAbsolutePath());
        browser.waitForVisible(CLOSE_BUTTON_LOCATOR);
        browser.click(CLOSE_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        browser.waitForVisible(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())));
    }

    public void moveFileToTrash(File attachedFile) {
        browser.waitForPresent(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())));
        browser.waitForPresent(TRASH_LOCATOR);
        browser.dragAndDrop(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())), TRASH_LOCATOR);
        browser.waitForAjaxProcessed();
    }

    public boolean isFilePresent(File attachedFile) {
        try {
            browser.waitForVisible(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void downloadFile(File attachedFile) {
        TimeOut.sleep(2);
        browser.waitForAjaxProcessed();
        browser.waitForPresent(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())));
        browser.click(By.xpath(String.format(FILE_LINK_PATTERN, attachedFile.getName())));
        TimeOut.sleep(2);
        browser.waitForAjaxProcessed();
        browser.waitForPresent(By.xpath(String.format(DOWNLOAD_BUTTON_PATTERN, attachedFile.getName())));
        browser.click(By.xpath(String.format(DOWNLOAD_BUTTON_PATTERN, attachedFile.getName())));
    }
}
