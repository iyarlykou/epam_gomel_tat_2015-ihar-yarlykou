package com.epam.gomel.tat.lesson_9.service;

import com.epam.gomel.tat.lesson_9.pages.DiskBasePage;
import com.epam.gomel.tat.lesson_9.pages.DiskTrashPage;

import java.io.File;

public class DiskGuiService {

    public void uploadFile(File attachedFile) {
        DiskBasePage diskBasePage = new DiskBasePage();
        diskBasePage.open().uploadFile(attachedFile);
    }

    public void moveFileToTrash(File attachedFile) {
        DiskBasePage diskBasePage = new DiskBasePage();
        diskBasePage.open().moveFileToTrash(attachedFile);
    }

    public void restoreFileFromTrash(File attachedFile) {
        DiskTrashPage diskTrashPage = new DiskTrashPage();
        diskTrashPage.open().restoreFile(attachedFile);
    }

    public boolean isFileRestore(File attachedFile) {
        DiskBasePage diskBasePage = new DiskBasePage();
        return diskBasePage.open().isFilePresent(attachedFile);
    }

    public void compareFiles(File attachedFile) {
        DiskBasePage diskBasePage = new DiskBasePage();
        diskBasePage.open().downloadFile(attachedFile);
        new DiskFileCompareService().compareFiles(attachedFile);
    }
}
