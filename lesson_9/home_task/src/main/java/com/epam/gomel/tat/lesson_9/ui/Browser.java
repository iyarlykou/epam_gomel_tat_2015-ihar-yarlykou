package com.epam.gomel.tat.lesson_9.ui;

import atu.testng.reports.ATUReports;
import com.epam.gomel.tat.lesson_9.cli.GlobalOptions;
import com.epam.gomel.tat.lesson_9.reporting.AtuLogger;
import com.epam.gomel.tat.lesson_9.reporting.Logger;
import com.google.common.base.Predicate;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Browser {

    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 40;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    private static final int AJAX_TIMEOUT = 20;
    private static final int WAIT_ELEMENT_TIMEOUT = 50;
    public static final String SCREENSHOT_DIRECTORY = "./screenshots";
    private static final String SCREENSHOT_LINK_PATTERN = "<a href=\"file:///%s\">Screenshot</a>";
    private static final String URL_PATH = "http://" + GlobalOptions.getRemoteHost() + ":" + GlobalOptions.getRemotePort() + "/wd/hub";
    private WebDriver driver;
    private static Map<Thread, Browser> instances = new HashMap<Thread, Browser>();
    private boolean augmented;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = GlobalOptions.getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                Logger.info("Browser REMOTE open");
                try {
                    driver = new RemoteWebDriver(new URL(URL_PATH),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                Logger.info("Browser FIREFOX open");
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case CHROME:
                Logger.info("Browser CHROME open");
                System.setProperty("webdriver.chrome.driver", GlobalOptions.getChromeDriver());
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("download.default_directory", GlobalOptions.DOWNLOAD_DIR);
                ChromeOptions options = new ChromeOptions();
                HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
                options.setExperimentalOption("prefs", chromePrefs);
                DesiredCapabilities cap = DesiredCapabilities.chrome();
                cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
                cap.setCapability(ChromeOptions.CAPABILITY, options);
                driver = new ChromeDriver(cap);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case HTMLUNIT:
                Logger.info("Browser HTMLUNIT open");
                DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
                driver = new HtmlUnitDriver(capabilities);
                ((HtmlUnitDriver) driver).setJavascriptEnabled(true);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
        }
        ATUReports.setWebDriver(driver);
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GlobalOptions.DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void configImplicitWait(int timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public void open(String url) {
        Logger.debug("Open Url: " + url);
        AtuLogger.info("Open Url: " + url);
        driver.get(url);
        takeScreenshot();
    }

    public static void kill() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void elementHighlight(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(
                "arguments[0].setAttribute('style', arguments[1]);",
                element, "background-color: yellow; border: 3px solid red;");
    }

    public void click(By locator) {
        Logger.info("Click: '" + locator.toString() + "'");
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.click();
        takeScreenshot();
    }

    public void type(By locator, String text) {
        Logger.info("Type: " + text + " into '" + locator.toString() + "'");
        AtuLogger.info("Type: " + text + " into '" + locator.toString() + "'");
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        element.sendKeys(text);
        takeScreenshot();
    }

    public String getText(By locator) {
        Logger.info("Get text from: " + locator);
        return driver.findElement(locator).getText();
    }

    public void attachFile(By locator, String text) {
        Logger.info("Attach file: " + text);
        driver.findElement(locator).sendKeys(text);
    }

    public boolean isPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.info("Wait for present: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.info("Wait for visible: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        Logger.info("Wait for Ajax processed");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void submit(By locator) {
        Logger.info("Submit: " + locator);
        driver.findElement(locator).submit();
    }

    public void takeScreenshot() {
        if (GlobalOptions.getBrowserType() == BrowserType.REMOTE &&
                !augmented) {
            driver = new Augmenter().augment(driver);
            augmented = true;
        }
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File copy = new File(SCREENSHOT_DIRECTORY + "/" + System.nanoTime() + ".png");
            FileUtils.copyFile(screenshot, copy);
            Logger.info(String.format(SCREENSHOT_LINK_PATTERN, copy.getAbsolutePath().replace("\\", "/")));
        } catch (IOException e) {
            Logger.error("Failed to copy screenshot", e);
        }
    }

    public void dragAndDrop(By locator, int xOffset, int yOffset) {
        WebElement draggable = driver.findElement(locator);
        new Actions(driver).dragAndDropBy(draggable, xOffset, yOffset).build().perform();
    }

    public void dragAndDrop(By dragLocator, By dropLocator) {
        Logger.info("DragAndDrop: '" + dragLocator.toString() + "'" + "   '" + dropLocator.toString() + "'");
        WebElement draggable = driver.findElement(dragLocator);
        WebElement droppable = driver.findElement(dropLocator);
        elementHighlight(draggable);
        elementHighlight(droppable);
        new Actions(driver).dragAndDrop(draggable, droppable).build().perform();
        takeScreenshot();
    }

    public void doubleClick(By locator) {
        Logger.info("DoubleClick: '" + locator.toString() + "'");
        WebElement element = driver.findElement(locator);
        elementHighlight(element);
        new Actions(driver).doubleClick(element).build().perform();
        takeScreenshot();
    }
}
