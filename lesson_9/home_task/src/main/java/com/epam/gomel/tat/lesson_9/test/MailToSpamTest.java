package com.epam.gomel.tat.lesson_9.test;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.lesson_9.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_9.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_9.service.LoginGuiService;
import com.epam.gomel.tat.lesson_9.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MailToSpamTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getDefaultMailLetterWithAttach();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @BeforeClass(description = "Send mail", dependsOnMethods = "loginToAccount")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Mail to Spam")
    public void mailToSpam() {
        MailboxBasePage mailbox = new MailboxBasePage()
                .openInboxPage()
                .messageToSpam(letter.getSubject());
    }

    @Test(description = "Check mail in spam list", dependsOnMethods = "mailToSpam")
    public void checkMailInSpamList() {
        Assert.assertTrue(mailGuiService.isMailInSpamList(letter));
    }
}
