package com.epam.gomel.tat.lesson_9.service;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_9.pages.DiskBasePage;
import com.epam.gomel.tat.lesson_9.pages.PassportLoginPage;
import com.epam.gomel.tat.lesson_9.reporting.Logger;

public class DiskLoginGuiService {

    public void loginToDiskAccount(Account account) {
        Logger.info("Login to account: " + account);
        new PassportLoginPage().open().login(account.getLogin(), account.getPassword());
        new DiskBasePage().open();

       /* String userEmail = mailbox.getUserEmail();
        if (userEmail == null || !userEmail.equals(account.getEmail())) {
            Logger.error("Login failed. User Mail : '" + userEmail + "'");
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }*/

    }
}
