package com.epam.gomel.tat.lesson_9.test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import atu.testng.reports.utils.Utils;
import com.epam.gomel.tat.lesson_9.utils.FileUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

@Listeners({ATUReportsListener.class, ConfigurationListener.class,
        MethodListener.class})
public class BaseTest {

    private static final String ATU_PROPERTIES_PATH = FileUtils.getCanonicalPathToResourceFile("/atu.properties");
    private static final String AUTHOR = "Ihar Yarlykou";
    private static final String VERSION = "1.0";
    private static final String PAGE_DESCRIPTION = "Yandex Test Descriptions";

    {
        System.setProperty("atu.reporter.config", ATU_PROPERTIES_PATH);
    }

    @BeforeClass
    public void init() {
        setIndexPageDescription();
    }

    private void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo(AUTHOR, Utils.getCurrentTime(),
                VERSION);
    }

    private void setIndexPageDescription() {
        ATUReports.indexPageDescription = PAGE_DESCRIPTION;
    }
}
