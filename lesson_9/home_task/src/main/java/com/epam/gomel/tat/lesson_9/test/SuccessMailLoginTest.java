package com.epam.gomel.tat.lesson_9.test;

import com.epam.gomel.tat.lesson_9.bo.common.Account;
import com.epam.gomel.tat.lesson_9.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_9.service.LoginGuiService;
import org.testng.annotations.Test;

public class SuccessMailLoginTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();

    @Test(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }
}
