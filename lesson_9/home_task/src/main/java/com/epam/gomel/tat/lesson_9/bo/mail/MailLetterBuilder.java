package com.epam.gomel.tat.lesson_9.bo.mail;

import com.epam.gomel.tat.lesson_9.utils.FileUtils;
import com.epam.gomel.tat.lesson_9.utils.RandomUtils;

import java.io.File;

public class MailLetterBuilder {

    private static final String mailTo = "igorepamtest@yandex.ru";
    private static File attachedFile = FileUtils.createRandomFile();

    public static MailLetter getDefaultMailLetterWithAttach() {
        MailLetter letter = new MailLetter(mailTo, RandomUtils.getRandomMailSubject(), RandomUtils.getRandomMailContent(), attachedFile.getAbsolutePath());
        return letter;
    }

    public static MailLetter getDefaultMailLetterWithoutAttach() {
        MailLetter letter = new MailLetter(mailTo, RandomUtils.getRandomMailSubject(), RandomUtils.getRandomMailContent());
        return letter;
    }

}
