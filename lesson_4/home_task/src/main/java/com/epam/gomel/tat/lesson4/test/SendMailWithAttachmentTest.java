package com.epam.gomel.tat.lesson4.test;

import com.epam.gomel.tat.lesson4.utils.FileUtils;
import com.epam.gomel.tat.lesson4.utils.RandomUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SendMailWithAttachmentTest {

    //Base URL
    public static final String BASE_URL = "http://www.ya.ru";

    //e-mail of receiver
    public static final String DEST_MAIL_ADRESS = "igorepamtest@yandex.ru";

    // Login and password
    public static final String userLogin = "igorepamtest";
    public static final String userPassword = "qwertyqwerty";

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 30;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;
    private File attachFile;
    private File downloadedFile;

    // UI Locators
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_BUTTON_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By INBOX_MAIL_SUBJ_TEXT_LOCATOR = By.xpath("//span[@class='b-messages__subject']");
    public static final By ATT_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    public static final By DOWNLOAD_ATT_FILE_LOCATOR = By.xpath("//span[@class='b-messages__attachments__count__inner']");
    public static final By DOWNLOAD_BUTTON = By.xpath("//div[@class='b-messages__attachments__popup__inner__footer']/a");

    //Content data
    private String mailSubject = RandomUtils.getRandomMailSubject();
    private String mailContent = RandomUtils.getRandomMailContent();
    private String saveFilePath = "D:\\repository\\epam_gomel_tat_2015-ihar-yarlykou\\lesson_4\\home_task\\save\\";

    @BeforeClass(description = "Prepare browser profile and timeouts")
    public void prepareBrowser() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", saveFilePath);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/xml,text/plain,text/xml,image/jpeg");
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Login to mail")
    public void loginToAccountMailbox() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
    }

    @Test(description = "Sending mail", dependsOnMethods = "loginToAccountMailbox")
    public void SendMail() {
        attachFile = FileUtils.createRandomFile();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(DEST_MAIL_ADRESS);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement attachFileButton = driver.findElement(ATT_BUTTON_LOCATOR);
        attachFileButton.sendKeys(attachFile.getAbsolutePath());
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    @Test(description = "Download Arrived Attached File", dependsOnMethods = "SendMail")
    public void downloadAttachedFile() throws InterruptedException {
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(INBOX_BUTTON_LOCATOR));
        WebElement inboxButton = driver.findElement(INBOX_BUTTON_LOCATOR);
        inboxButton.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(INBOX_MAIL_SUBJ_TEXT_LOCATOR));
        WebElement inboxSubjText = driver.findElement(INBOX_MAIL_SUBJ_TEXT_LOCATOR);
        inboxSubjText.click();
        WebElement attFile = driver.findElement(DOWNLOAD_ATT_FILE_LOCATOR);
        attFile.click();
        WebElement downloadFileButton = driver.findElement(DOWNLOAD_BUTTON);
        downloadFileButton.click();
        downloadedFile = new File(saveFilePath + attachFile.getName());
        FileUtils.waitForFile(downloadedFile);
    }

    @Test(description = "Compare files content", dependsOnMethods = "downloadAttachedFile")
    public void compareFilesContent() throws IOException {
        FileUtils.compareFileConten(attachFile, downloadedFile);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }
}
