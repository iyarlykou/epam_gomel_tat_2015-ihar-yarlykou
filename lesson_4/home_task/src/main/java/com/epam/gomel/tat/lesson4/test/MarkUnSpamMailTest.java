package com.epam.gomel.tat.lesson4.test;

import com.epam.gomel.tat.lesson4.utils.RandomUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MarkUnSpamMailTest {
    //Base URL
    public static final String BASE_URL = "http://www.ya.ru";

    //e-mail of receiver
    public static final String DEST_MAIL_ADRESS = "igorepamtest@yandex.ru";

    // Login and password
    public static final String userLogin = "igorepamtest";
    public static final String userPassword = "qwertyqwerty";

    //Content data
    private String mailSubject = RandomUtils.getRandomMailSubject();
    private String mailContent = RandomUtils.getRandomMailContent();

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 30;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    private WebDriver driver;
    public String subj_text_inbox;

    // UI Locators
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By INBOX_BUTTON_LOCATOR = By.xpath("//div[@class='block-left-box']//span[@class='b-folders__folder__name']/a[@href='#inbox']");
    public static final By INBOX_MAIL_SUBJ_TEXT_LOCATOR = By.xpath("//span[@class='b-messages__subject']");
    public static final By SPAMBOX_BUTTON_LOCATOR = By.xpath("//div[@class='block-left-box']//span[@class='b-folders__folder__name']/a[@href='#spam']");
    public static final By SPAMBOX_MAIL_SUBJ_TEXT_LOCATOR = By.xpath("//span[@class='b-messages__firstline-wrapper']/span[@class='b-messages__subject']");
    public static final By SPAMBOX_MAIL_CHECK_LOCATOR = By.xpath("//label[@class='b-messages__message__checkbox']//input[@type='checkbox']");
    public static final By UNSPAM_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='notspam']");
    public static final By INBOX_MAIL_CHECK_LOCATOR = By.xpath("//label[@class='b-messages__message__checkbox']//input[@type='checkbox']");
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='tospam']");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    @BeforeClass(description = "Prepare browser timeouts")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @BeforeClass(description = "Login to mail", dependsOnMethods = "prepareBrowser")
    public void loginToAccountMailbox() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement inboxButton = driver.findElement(INBOX_BUTTON_LOCATOR);
        inboxButton.click();
    }

    @BeforeClass(description = "Send mail", dependsOnMethods = "loginToAccountMailbox")
    public void sendMail() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(DEST_MAIL_ADRESS);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    @Test(description = "Mail to spam")
    public void MailToSpam() {
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(INBOX_BUTTON_LOCATOR));
        WebElement inboxButton = driver.findElement(INBOX_BUTTON_LOCATOR);
        inboxButton.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(INBOX_MAIL_SUBJ_TEXT_LOCATOR));
        WebElement inboxSubjText = driver.findElement(INBOX_MAIL_SUBJ_TEXT_LOCATOR);
        subj_text_inbox = inboxSubjText.getText();
        WebElement inboxMailCheck = driver.findElement(INBOX_MAIL_CHECK_LOCATOR);
        inboxMailCheck.click();
        WebElement spamButton = driver.findElement(SPAM_BUTTON_LOCATOR);
        spamButton.click();
    }

    @Test(description = "Check message in SpamBox", dependsOnMethods = "MailToSpam")
    public void CheckSpamMessage() {
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(SPAMBOX_BUTTON_LOCATOR));
        WebElement spamboxButton = driver.findElement(SPAMBOX_BUTTON_LOCATOR);
        spamboxButton.click();
        WebElement spamSubjText = driver.findElement(SPAMBOX_MAIL_SUBJ_TEXT_LOCATOR);
        Assert.assertEquals(spamSubjText.getText(), subj_text_inbox);
    }

    @Test(description = "Mail to UNspam", dependsOnMethods = "CheckSpamMessage")
    public void MailToUnSpam() {
        WebElement spamboxMailCheck = driver.findElement(SPAMBOX_MAIL_CHECK_LOCATOR);
        spamboxMailCheck.click();
        WebElement unspamButton = driver.findElement(UNSPAM_BUTTON_LOCATOR);
        unspamButton.click();
    }

    @Test(description = "Check message in Inbox", dependsOnMethods = "MailToUnSpam")
    public void CheckInboxMessage() {
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(SPAMBOX_BUTTON_LOCATOR));
        WebElement inboxButton = driver.findElement(INBOX_BUTTON_LOCATOR);
        inboxButton.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(INBOX_MAIL_SUBJ_TEXT_LOCATOR));
        WebElement inboxMailSubjText = driver.findElement(INBOX_MAIL_SUBJ_TEXT_LOCATOR);
        Assert.assertEquals(inboxMailSubjText.getText(), subj_text_inbox);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

}
