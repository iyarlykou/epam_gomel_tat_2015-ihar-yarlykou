package com.epam.gomel.tat.lesson4.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class RandomUtils {
    public static final int SUBJ_LENGTH = 20;
    public static final int MAIL_CONTEN_LENGTH = 60;

    public static String getRandomMailSubject() {
        return RandomStringUtils.randomAlphanumeric(SUBJ_LENGTH);
    }

    public static String getRandomMailContent() {
        return RandomStringUtils.randomAlphanumeric(MAIL_CONTEN_LENGTH);
    }
}
