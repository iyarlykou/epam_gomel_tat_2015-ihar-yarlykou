Test Case : Success login to yandex mail
Steps :
1. open http://www.yandex.ru/
2. Click button "Войти"
3. Login input - type correct login name
4. Password input - type correct password
5. Click button "Войти"
6. Validate success login (mail account link shold be on the top-right of page)


Test Case : Unsuccess login to yandex mail : incorrect password
Steps :
1. open http://www.yandex.ru/
2. Click button "Войти"
3. Login input - type correct login name
4. Password input - type INcorrect password
5. Click button "Войти"
6. Validate UNsuccess login (message with text "Неправильная пара логин-пароль! Авторизоваться не удалось." should be on page)

Test Case : Success send email
Steps :
1. Open account mail inbox
2. Click send mail
3. Fill inputs : TO, SUBJECT, MAIL CONTENT
4. Send the mail
5. Open mail sended messages list
6. Sent mail should be in list

Test Case : Success send email with attachment
Steps :
1. Open account mail inbox
2. Click send mail
3. Fill inputs : TO, SUBJECT, MAIL CONTENT
4. Send the mail
5. Open mail sended messages list
6. Open the sent mail
7. Download attached file
8. Check content of attached file - content should be the same 

Test Case : Delete email
Precondition : There is a new email message in account inbox
Steps :
1. Open account mail inbox
2. Delete the message
3. Open deleted messages list
4. The deleted message should be in list

Test Case : Mark email as a spam
Precondition : There is a new email message in account inbox
Steps :
1. Open account mail inbox
2. Mark message as Spam
3. Check message in Spam list

Test Case : Mark email as not spam
Precondition : There is a new email message in account inbox
Steps :
1. Open account mail inbox
2. Mark message as Spam
3. Open spam list
4. Mark message as not a spam
5. Check message disappear in spam list 
6. Check message is in inbox


Test Case :  Yandex Disk : create new folder
Test Case : Yandex Disk : upload file
Test Case : Yandex Disk : delete file
