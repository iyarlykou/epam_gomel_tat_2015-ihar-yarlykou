package com.epam.gomel.tat.lesson_6.service;

import com.epam.gomel.tat.lesson_6.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_6.pages.AbstractPage;
import com.epam.gomel.tat.lesson_6.pages.LetterContentPage;
import com.epam.gomel.tat.lesson_6.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_6.reporting.Logger;
import org.openqa.selenium.By;

import java.io.IOException;

public class MailGuiService {

    public void sendMail(MailLetter letter) {
        Logger.info("Sending mail to" + letter.getMailTo());
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getMailTo(), letter.getSubject(), letter.getMailContent(), letter.getAttach());
    }

    public boolean checkMailInSentList(MailLetter letter) throws IOException {
        Logger.info("Check mail in sent list");
        new MailboxBasePage().openSentPage().openMailPage(letter.getSubject());
        new LetterContentPage().compareMailContent(letter);
        return true;
    }

    public boolean checkMailInSpamList(MailLetter letter) {
        Logger.info("Check mail in spam list");
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openSpamPage().isMessagePresentInSpamList(letter.getSubject());
        return true;
    }

    public boolean checkMailInDeleteList(MailLetter letter) {
        Logger.info("Check mail in delete list");
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openDeletePage().isMessagePresentInDeleteList(letter.getSubject());
        return true;
    }

    public boolean checkMailInInboxList(MailLetter letter) {
        Logger.info("Check mail in inbox list");
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().isMessagePresentInInboxList(letter.getSubject());
        return true;
    }

    public boolean compareAttach(MailLetter letter) throws IOException {
        Logger.info("Compare files");
        new MailboxBasePage().openSentPage().openMailPage(letter.getSubject());
        new LetterContentPage().compareFiles(letter);
        return true;
    }
}
