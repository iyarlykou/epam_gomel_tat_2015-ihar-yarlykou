package com.epam.gomel.tat.lesson_6.pages;

import org.openqa.selenium.By;
import org.testng.Assert;

public class MailInboxListPage extends AbstractPage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='tospam']");
    public static final String CHECKBOX_MAIL_LOCATOR_PATTERN = "//span[@title='%s']/ancestor::div[1]//input[@type='checkbox']";
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='delete']");
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//a[.//*[text()='%s']]";

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailboxBasePage messageToSpam(String subject) {
        browser.waitForVisible(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }

    public MailboxBasePage messageToDelete(String subject) {
        browser.waitForVisible(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(By.xpath(String.format(CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(DELETE_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }

    public boolean isMessagePresentInInboxList(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }
}
