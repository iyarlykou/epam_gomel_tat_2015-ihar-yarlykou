package com.epam.gomel.tat.lesson_6.pages;

import org.openqa.selenium.By;

public class MailDeleteListPage extends AbstractPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN = "//label[text()='Удалённые']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";

    public boolean isMessagePresentInDeleteList(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }
}
