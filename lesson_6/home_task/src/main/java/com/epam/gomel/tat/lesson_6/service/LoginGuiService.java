package com.epam.gomel.tat.lesson_6.service;

import com.epam.gomel.tat.lesson_6.bo.common.Account;
import com.epam.gomel.tat.lesson_6.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_6.pages.MailLoginPage;
import com.epam.gomel.tat.lesson_6.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_6.reporting.Logger;
import com.epam.gomel.tat.lesson_6.ui.Browser;
import org.testng.Assert;
import com.epam.gomel.tat.lesson_6.reporting.Logger;

public class LoginGuiService {

    static final String ERROR_TEXT = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public void loginToAccountMailbox(Account account) {
        Logger.info("Login to account: " + account);
        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null || !userEmail.equals(account.getEmail())) {
            Logger.error("Login failed. User Mail : '" + userEmail + "'");
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }

    public void loginToWrongAccount(Account account) {
        Logger.info("Login to account with wrong password: " + account);
        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        if (!mailbox.getMessage().equals(ERROR_TEXT)) {
            Logger.error("Error message is not found");
            throw new TestCommonRuntimeException("Error message is not found");
        }
    }
}
