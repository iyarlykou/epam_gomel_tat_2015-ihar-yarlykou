package com.epam.gomel.tat.lesson_6.bo.common;

public class AccountBuilder {

    private static final String LOGIN = "igorepamtest";
    private static final String PASSWORD = "qwertyqwerty";
    private static final String EMAIL = "igorepamtest@yandex.ru";
    private static final String INCORRECT_PASS = "beleberda223634735";

    public static Account getDefaultAccount() {
        Account account = new Account(LOGIN, PASSWORD, EMAIL);
        return account;
    }

    public static Account getAccountWithWrongPassword() {
        Account account = new Account(LOGIN, INCORRECT_PASS, EMAIL);
        return account;
    }
}
