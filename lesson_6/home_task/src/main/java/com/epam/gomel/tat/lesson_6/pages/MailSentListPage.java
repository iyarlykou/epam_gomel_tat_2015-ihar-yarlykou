package com.epam.gomel.tat.lesson_6.pages;

import com.epam.gomel.tat.lesson_6.ui.Browser;
import com.epam.gomel.tat.lesson_6.utils.FileUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;

public class MailSentListPage extends AbstractPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN = "//label[text()='Отправленные']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }

    public void openMailPage(String subject) {
        browser.click(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
    }
}
