package com.epam.gomel.tat.lesson_6.bo.mail;

public class MailLetter {

    private String receiver;
    private String subject;
    private String content;
    private String attach;
    private String fileName;

    public MailLetter(String receiver, String subject, String content, String attach, String fileName) {
        this.receiver = receiver;
        this.subject = subject;
        this.content = content;
        this.attach = attach;
        this.fileName = fileName;
    }

    public MailLetter(String receiver, String subject, String content) {
        this.receiver = receiver;
        this.subject = subject;
        this.content = content;
    }

    public String getMailTo() {
        return receiver;
    }

    public void setMailTo(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMailContent() {
        return content;
    }

    public void setMailContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getFileName() {
        return fileName;
    }
}
