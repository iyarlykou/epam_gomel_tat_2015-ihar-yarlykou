package com.epam.gomel.tat.lesson_6.pages;

import org.openqa.selenium.By;

public class ComposeMailPage extends AbstractPage {

    private static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By ATTACHE_FILE_INPUT_LOCATOR = By.xpath("//input[@name='att']");

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent, String attach) {
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, mailSubject);
        browser.type(MAIL_TEXT_LOCATOR, mailContent);
        if (attach != null) {
            browser.attachFile(ATTACHE_FILE_INPUT_LOCATOR, attach);
        }
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }
}
