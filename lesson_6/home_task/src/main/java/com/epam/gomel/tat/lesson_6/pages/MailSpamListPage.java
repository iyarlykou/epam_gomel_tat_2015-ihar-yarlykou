package com.epam.gomel.tat.lesson_6.pages;

import org.openqa.selenium.By;

public class MailSpamListPage extends AbstractPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN = "//label[text()='Спам']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";
    public static final By UNSPAM_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[@data-action='notspam']");
    private static final String SPAM_CHECKBOX_MAIL_LOCATOR_PATTERN = "//label[text() = 'Спам']/ancestor::div[@class = 'block-messages']//span[@class = 'b-messages__subject'][text()='%s']/ancestor::div[1]//input[@type = 'checkbox']";

    public MailboxBasePage messageToUnSpam(String subject) {
        browser.waitForAjaxProcessed();
        browser.waitForVisible(By.xpath(String.format(SPAM_CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(By.xpath(String.format(SPAM_CHECKBOX_MAIL_LOCATOR_PATTERN, subject)));
        browser.click(UNSPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }

    public boolean isMessagePresentInSpamList(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }
}
