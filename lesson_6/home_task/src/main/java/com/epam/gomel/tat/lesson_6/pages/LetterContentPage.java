package com.epam.gomel.tat.lesson_6.pages;

import com.epam.gomel.tat.lesson_6.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_6.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_6.reporting.Logger;
import com.epam.gomel.tat.lesson_6.ui.Browser;
import com.epam.gomel.tat.lesson_6.utils.FileUtils;
import org.openqa.selenium.By;

import java.io.File;
import java.io.IOException;

public class LetterContentPage extends AbstractPage {

    public static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//a[@class='b-link b-link_w b-link_js b-file__download js-attachments-get-btn daria-action']");
    public static final By MAIL_CONTENT_LOCATOR = By.xpath("//div[@class='b-message__i1']//div[@class='b-message-body__content']/p");
    public static final By EMAIL_LOCATOR = By.xpath("//span[@class='b-message-head__name']//span[@class='b-message-head__email']");
    public static final By MAIL_SUBJECT_LOCATOR = By.xpath("//span[@class='js-message-subject js-invalid-drag-target']");

    public void compareFiles(MailLetter letter) throws IOException {
        browser.click(DOWNLOAD_BUTTON_LOCATOR);
        File downloadedFile = new File(Browser.DOWNLOAD_DIR + letter.getFileName());
        File attachFile = new File(letter.getAttach());
        FileUtils.waitForFile(downloadedFile);
        FileUtils.compareFileConten(attachFile, downloadedFile);
    }

    public void compareMailContent(MailLetter letter) {
        String mailToFormat = " <" + letter.getMailTo() + ">";
        StringBuilder builder = new StringBuilder();
        if (!letter.getSubject().equals(browser.getText(MAIL_SUBJECT_LOCATOR))) {
            builder.append("Mail subjects is not equals  " + "Expected subject: " + letter.getSubject() + " Actual subject: " + browser.getText(MAIL_SUBJECT_LOCATOR));
        }
        if (!letter.getMailContent().equals(browser.getText(MAIL_CONTENT_LOCATOR))) {
            builder.append("Mail contents is not equals  " + "Expected content: " + letter.getMailContent() + "  Actual content: " + browser.getText(MAIL_CONTENT_LOCATOR));
        }
        if (!mailToFormat.equals(browser.getText(EMAIL_LOCATOR))) {
            builder.append("Emails is not equals  " + "Expected email: " + mailToFormat + "  Actual email: " + browser.getText(EMAIL_LOCATOR));
        }
        if (!builder.toString().equals("")) {
            Logger.info(builder.toString());
            Logger.error(builder.toString());
            throw new TestCommonRuntimeException(builder.toString());
        }
    }
}
