package com.epam.gomel.tat.lesson_6.bo.mail;

import com.epam.gomel.tat.lesson_6.bo.common.Account;
import com.epam.gomel.tat.lesson_6.utils.FileUtils;
import com.epam.gomel.tat.lesson_6.utils.RandomUtils;

import java.io.File;

public class MailLetterBuilder {

    private static final String mailTo = "igorepamtest@yandex.ru";
    private static final String mailSubject = RandomUtils.getRandomMailSubject();
    private static final String mailContent = RandomUtils.getRandomMailContent();
    private static File attachedFile = FileUtils.createRandomFile();
    private static final String filePath = attachedFile.getAbsolutePath();
    private static final String fileName = attachedFile.getName();


    public static MailLetter getDefaultMailLetter() {
        MailLetter letter = new MailLetter(mailTo, mailSubject, mailContent, filePath, fileName);
        return letter;
    }

    public String getmailTo() {
        return mailTo;
    }

    public String getSubject() {
        return mailSubject;
    }

    public String getmailContent() {
        return mailContent;
    }

    public String getAttach() {
        return filePath;
    }

    public String getfileName() {
        return fileName;
    }
}
