package com.epam.gomel.tat.lesson_6.test;

import com.epam.gomel.tat.lesson_6.bo.common.Account;
import com.epam.gomel.tat.lesson_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_6.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.lesson_6.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_6.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_6.service.LoginGuiService;
import com.epam.gomel.tat.lesson_6.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DeleteMailTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getDefaultMailLetter();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @BeforeClass(description = "Send mail", dependsOnMethods = "loginToAccount")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Mail to delete List")
    public void mailToDelete() {
        MailboxBasePage mailbox = new MailboxBasePage()
                .openInboxPage()
                .messageToDelete(letter.getSubject());
    }

    @Test(description = "Check mail in delete list", dependsOnMethods = "mailToDelete")
    public void checkMailInDeleteList() {
        Assert.assertTrue(mailGuiService.checkMailInDeleteList(letter));
    }
}
