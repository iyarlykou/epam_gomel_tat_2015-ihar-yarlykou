package com.epam.gomel.tat.lesson_6.runner;

import com.epam.gomel.tat.lesson_6.reporting.CustomTestNgListener;
import com.epam.gomel.tat.lesson_6.reporting.Logger;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        Logger.info("Test start");
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
        for (String arg : args) {
            Logger.info("Run suit:" + arg);
            suites.add(arg);
        }
        testNG.addListener(new CustomTestNgListener());
        testNG.setTestSuites(suites);
        testNG.run();
        Logger.info("Test finish");
    }
}
