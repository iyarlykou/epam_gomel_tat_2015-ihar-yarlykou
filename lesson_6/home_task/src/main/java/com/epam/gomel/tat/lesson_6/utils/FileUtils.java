 package com.epam.gomel.tat.lesson_6.utils;

import com.google.common.base.Function;
import com.sun.istack.internal.Nullable;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public class FileUtils {
    public static int WAIT_FILE_DOWNLOAD_SECONDS = 30;
    public static final int FILE_NAME_LENGTH = 10;
    public static final int FILE_CONTEN_LENGTH = 60;

    public static void waitForFile(File filename) {
        FluentWait wait = new FluentWait<File>(filename).withTimeout(WAIT_FILE_DOWNLOAD_SECONDS, TimeUnit.SECONDS);
        wait.until(new Function<File, Boolean>() {
            @Nullable
            @Override
            public Boolean apply(File file) {
                return file.exists();
            }
        });
    }

    public static String getRandomFileContent() {
        return RandomStringUtils.randomAlphanumeric(FILE_CONTEN_LENGTH);
    }

    public static String getRandomFileName() {
        return RandomStringUtils.randomAlphanumeric(FILE_NAME_LENGTH);
    }

    public static File createRandomFile() {
        String fileName;
        String fileContent;
        fileName = getRandomFileName() + ".txt";
        fileContent = getRandomFileContent();
        File file = new File(fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintWriter out = new PrintWriter(file);
            try {
                out.print(fileContent);
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    public static void compareFileConten(File file1, File file2) throws IOException
    {
   org.apache.commons.io.FileUtils.contentEquals(file1, file2);}

}
