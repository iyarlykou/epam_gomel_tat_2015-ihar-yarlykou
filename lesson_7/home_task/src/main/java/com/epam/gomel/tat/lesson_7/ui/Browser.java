package com.epam.gomel.tat.lesson_7.ui;

import com.epam.gomel.tat.lesson_7.cli.GlobalOptions;
import com.epam.gomel.tat.lesson_7.reporting.Logger;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Browser {

    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    private static final int AJAX_TIMEOUT = 20;
    private static final int WAIT_ELEMENT_TIMEOUT = 20;
    private static final String URL_PATH = "http://" + GlobalOptions.getRemoteHost() + ":" + GlobalOptions.getRemotePort() + "/wd/hub";
    private WebDriver driver;
    private static Map<Thread, Browser> instances = new HashMap<Thread, Browser>();
    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = init();
        instances.put(currentThread, instance);
        return instance;
    }

    private static Browser init() {
        BrowserType browserType = GlobalOptions.getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                Logger.info("Browser REMOTE open");
                try {
                    driver = new RemoteWebDriver(new URL(URL_PATH),
                            DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                Logger.info("Browser FIREFOX open");
                driver = new FirefoxDriver(getFireFoxProfile());
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case CHROME:
                Logger.info("Browser CHROME open");
                System.setProperty("webdriver.chrome.driver", GlobalOptions.getChromeDriver());
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("download.default_directory", GlobalOptions.DOWNLOAD_DIR);
                ChromeOptions options = new ChromeOptions();
                HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
                options.setExperimentalOption("prefs", chromePrefs);
                DesiredCapabilities cap = DesiredCapabilities.chrome();
                cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
                cap.setCapability(ChromeOptions.CAPABILITY, options);
                driver = new ChromeDriver(cap);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            case HTMLUNIT:
                Logger.info("Browser HTMLUNIT open");
                DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
                driver = new HtmlUnitDriver(capabilities);
                ((HtmlUnitDriver) driver).setJavascriptEnabled(true);
                driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                break;
        }

        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GlobalOptions.DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        return profile;
    }

    public void open(String url) {
        Logger.info("Open Url" + url);
        driver.get(url);
    }

    public static void kill() {
        Thread currentThread = Thread.currentThread();
        Browser instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public void click(By locator) {
        Logger.info("Click on: " + locator);
        driver.findElement(locator).click();
    }

    public void type(By locator, String text) {
        Logger.info("Type on: " + locator + "     Text: " + text);
        driver.findElement(locator).sendKeys(text);
    }

    public String getText(By locator) {
        Logger.info("Get text from: " + locator);
        return driver.findElement(locator).getText();
    }

    public void attachFile(By locator, String text) {
        Logger.info("Attach file: " + text);
        driver.findElement(locator).sendKeys(text);
    }

    public boolean isPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.info("Wait for present: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.info("Wait for visible: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        Logger.info("Wait for Ajax processed");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public void submit(By locator) {
        Logger.info("Submit: " + locator);
        driver.findElement(locator).submit();
    }
}
