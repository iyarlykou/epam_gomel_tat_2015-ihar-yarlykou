package com.epam.gomel.tat.lesson_7.test;

import com.epam.gomel.tat.lesson_7.bo.common.Account;
import com.epam.gomel.tat.lesson_7.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_7.service.LoginGuiService;
import org.testng.annotations.Test;

public class UnSuccessMailLoginTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account wrongAccount = AccountBuilder.getAccountWithWrongPassword();

    @Test(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToWrongAccount(wrongAccount);
    }
}
