package com.epam.gomel.tat.lesson_7.runner;

import com.epam.gomel.tat.lesson_7.cli.GlobalOptions;
import com.epam.gomel.tat.lesson_7.reporting.CustomTestNgListener;
import com.epam.gomel.tat.lesson_7.reporting.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import org.testng.xml.Parser;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    private static GlobalOptions globalOptions = null;

    public static void main(String[] args) {
        globalOptions = GlobalOptions.getInstance();
        Runner runner = new Runner();
        runner.parseCli(args);
        runner.runTests();
    }

    private void parseCli(String[] args) {

        CmdLineParser parser = new CmdLineParser(globalOptions);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            Logger.error("Can't parse arguments! " + e.getMessage());
            parser.printUsage(System.err);
            return;
        }
    }

    private void runTests() {
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNgListener());
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        for (String suiteFile : GlobalOptions.getSuitesFiles()) {
            try {
                List<XmlSuite> suitesToConfigure = (List<XmlSuite>) (new Parser(suiteFile).parse());
                for (XmlSuite suiteToConfigure : suitesToConfigure) {
                    suiteToConfigure.setParallel(GlobalOptions.getParallelMode());
                    suiteToConfigure.setThreadCount(GlobalOptions.getThreadCount());
                    suites.add(suiteToConfigure);
                }
            } catch (Exception e) {
                Logger.error("Can't set options to suites files!");
                e.printStackTrace();
            }
        }
        testNG.setXmlSuites(suites);
        testNG.run();
    }
}
