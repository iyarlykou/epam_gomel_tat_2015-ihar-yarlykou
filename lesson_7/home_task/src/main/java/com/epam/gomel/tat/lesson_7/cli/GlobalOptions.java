package com.epam.gomel.tat.lesson_7.cli;

import com.epam.gomel.tat.lesson_7.reporting.Logger;
import com.epam.gomel.tat.lesson_7.ui.BrowserType;
import com.epam.gomel.tat.lesson_7.utils.FileUtils;
import com.epam.gomel.tat.lesson_7.utils.ParallelMode;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import java.util.ArrayList;
import java.util.List;

public class GlobalOptions {

    @Option(name = "-driver", usage = "chromedriver")
    private static String chromeDriver = "src/main/resources/chromedriver.exe";

    @Option(name = "-suites", required = true, handler = StringArrayOptionHandler.class)
    private static List<String> suitesFiles = new ArrayList<String>();

    @Option(name = "-bt", usage = "browser type", required = true)
    private static BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-mode", usage = "parallel mode: false, tests, classes")
    private static ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-tc", usage = "thread count")
    private static int threadCount = 1;

    @Option(name = "-host", usage = "remote host")
    private static String remoteHost = "localhost";

    @Option(name = "-port", usage = "remote port")
    private static String remotePort = "4444";

    public static final String DOWNLOAD_DIR = org.apache.commons.io.FileUtils.getTempDirectoryPath();
    private static GlobalOptions instance = null;

    public static GlobalOptions getInstance() {
        Logger.info("Get options");
        if (instance == null) {
            instance = new GlobalOptions();
        }
        return instance;
    }

    public static BrowserType getBrowserType() {
        return browserType;
    }

    public static String getParallelMode() {
        return parallelMode.name().toLowerCase();
    }

    public static String getRemoteHost() {
        return remoteHost;
    }

    public static String getRemotePort() {
        return remotePort;
    }

    public static List<String> getSuitesFiles() {
        return suitesFiles;
    }

    public static int getThreadCount() {
        return threadCount;
    }

    public static String getChromeDriver() {
        return chromeDriver;
    }
}
