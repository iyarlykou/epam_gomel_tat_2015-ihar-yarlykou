package com.epam.gomel.tat.lesson_7.test;

import com.epam.gomel.tat.lesson_7.bo.common.Account;
import com.epam.gomel.tat.lesson_7.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_7.bo.mail.MailLetterBuilder;
import com.epam.gomel.tat.lesson_7.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_7.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_7.service.LoginGuiService;
import com.epam.gomel.tat.lesson_7.service.MailGuiService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MailToUnSpamTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();
    private MailLetter letter = MailLetterBuilder.getDefaultMailLetterWithAttach();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @BeforeClass(description = "Send mail", dependsOnMethods = "loginToAccount")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @BeforeClass(description = "Mail to Spam", dependsOnMethods = "sendMail")
    public void mailToSpam() {
        MailboxBasePage mailbox = new MailboxBasePage()
                .openInboxPage()
                .messageToSpam(letter.getSubject());
    }

    @Test(description = "Mail to UnSpam")
    public void mailToUnSpam() {
        MailboxBasePage mailbox = new MailboxBasePage()
                .openSpamPage()
                .messageToUnSpam(letter.getSubject());
    }

    @Test(description = "Check mail in inbox list", dependsOnMethods = "mailToUnSpam")
    public void checkMailInInboxList() {
        Assert.assertTrue(mailGuiService.isMailInInboxList(letter));
    }
}
