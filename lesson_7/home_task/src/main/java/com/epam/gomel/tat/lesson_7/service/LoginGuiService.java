package com.epam.gomel.tat.lesson_7.service;

import com.epam.gomel.tat.lesson_7.bo.common.Account;
import com.epam.gomel.tat.lesson_7.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_7.pages.MailLoginPage;
import com.epam.gomel.tat.lesson_7.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_7.reporting.Logger;

public class LoginGuiService {

    static final String ERROR_TEXT = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public void loginToAccountMailbox(Account account) {
        Logger.info("Login to account: " + account);
        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null || !userEmail.equals(account.getEmail())) {
            Logger.error("Login failed. User Mail : '" + userEmail + "'");
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }

    public void loginToWrongAccount(Account account) {
        Logger.info("Login to account with wrong password: " + account);
        MailboxBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String errorMessage = mailbox.getMessage();
        if (!errorMessage.equals(ERROR_TEXT)) {
            // Logger.error("Error message is not found. Current message: " + errorMessage);
            throw new TestCommonRuntimeException("Error message is not found. Current message: " + errorMessage);
        }
    }
}
